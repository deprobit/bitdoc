import { LightningElement } from 'lwc';
import Header_ManageUsers from '@salesforce/label/c.Header_ManageUsers';
import Resource_Logos from '@salesforce/resourceUrl/Resource_Logos';

export default class globalHeader extends LightningElement {
    label = {
        Header_ManageUsers            
    };
	Header_ManageUsers = Resource_Logos + '/Resource_Logos/images/ManageUsers.png'
	Header_MessageCenter = Resource_Logos + '/Resource_Logos/images/MessageCenter.png'
}