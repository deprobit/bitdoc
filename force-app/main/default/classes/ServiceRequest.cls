/**
* @author       
* @description  This is the wrapper for parsing Request.
*
* Modification Log
* ------------------------------------------------------------------------------------
*
**/
global class ServiceRequest {
    @AuraEnabled
    global String apexClass { get; set; }

    @AuraEnabled
    global String apexMethod { get; set; }

    @AuraEnabled
    global String payload { get; set; }
    
    /*
    * @author       
    * @description  For serializing the JSON.
    * @param        null
    * @return       String
    */  
    global String toJSON() {
        return JSON.serialize(this);
    }
    
    /*
    * @author       
    * @description  For deserializing the JSON.
    * @param        String strRequest
    * @return       ServiceRequest 
    */  
    global static ServiceRequest toObject(String strRequest) {
        return (ServiceRequest) JSON.deserializeStrict(
            strRequest,
            ServiceRequest.class
        );
    }
}
